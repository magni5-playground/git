# Laboratorio di progettazione

## Team

- Brugora Michael 851813
- Colombo Lorenzo 851918
- Locatelli Riccardo 852095
- Magni Marco 851810

## Title

LP.

> LP as the acronym of "Laboratorio di Progettazione" and "Long Playing", the format of a 33 rpm vinyl record.

## Abstract

Aim of the project is the development of a web application to manage and consult a catalogue of music albums.

Users will be able to contribute to the catalogue by adding a disc or other information related to it, such as metadata, ratings, and reviews.

## LLMs

The team will make use of ChatGPT or other equivalent generative AI models.

## Architectural spike

![LP's architectural spike](/resources/architectural_spike.jpg "LP's architectural spike")

## How-to

Check out the [how-to](how-to.md) files to learn how to setup the environments and run the applications correctly.