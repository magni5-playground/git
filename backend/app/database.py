'''
Module used to manage the connection between the application and the MongoDB database.
'''

import re
import os
import motor.motor_asyncio

from app.models.album import Album

client = motor.motor_asyncio.AsyncIOMotorClient(os.environ['MONGODB_URL'])
database = client.get_database('lp')
users_collection = database.get_collection('users')
albums_collection = database.get_collection('albums')

async def fetch_one_user_psw(username, password):
    '''
    Returns the user who meets the credentials.
    Returns: response: a JSON file containing informations about the user found.
    '''

    document = await users_collection.find_one({"username": username, "password":password})
    return document


async def fetch_albums(fltr: str):
    '''
    Returns all the albums in the database that satisfy the optional filter.
    
    Parameters: 
        fltr (str): the optional filter to apply to the album database.
    
    Returns:
        response: a JSON file containing informations about the albums found. 
    '''
    fltr = fltr if len(fltr) >= 3 else ''
    rgx = re.compile('.*' + fltr + '.*', re.IGNORECASE)
    albums = []
    cursor = albums_collection.find({
        '$or': [ { 'album_name': rgx }, { 'album_artists': rgx } ]
    })
    async for album in cursor:
        albums.append(Album(**album))
    return albums

async def fetch_one_user(username):
    '''
    Returns a user with a specific username
    
    Parameters: 
        username: the username to use to fetch the specific user
    
    Returns:
        response: a JSON file containing informations about the user found. 
    '''

    document = await users_collection.find_one({"username": username})
    return document

async def create_user(user):
    '''
    Adds a user to the collection
    
    Parameters: 
        user: a JSON file containing the user to add to the collection
    
    Returns:
        response: a JSON file containing the user added
    '''

    document = user
    await users_collection.insert_one(document)
    return document

async def update_user(username, password, email, name, surname):
    '''
    Updates the user who meets the username with the new data.
    Returns: response: a JSON file containing informations about the user modified.
    '''
    await users_collection.update_one({"username":username},
          {"$set": {"password": password, "email":email, "name":name, "surname":surname}})
    await users_collection.find_one({"username": username})

async def remove_user(username):
    '''
    Removes a user who meets the username.
    Returns: true if deleted.
    '''
    # Try to delete the document from MongoDB
    await users_collection.delete_one({"username": username})
    return True
