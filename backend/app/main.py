'''
Module used to manage all the calls at the backend.
'''
import os
import secrets
import jwt
import uvicorn
from fastapi import FastAPI, HTTPException, Request
from fastapi.middleware.cors import CORSMiddleware
from app.models.update import UpdateModel
from app.models.user import User
from app.database import (
    fetch_one_user_psw,
    fetch_one_user,
    create_user,
    fetch_albums,
    remove_user,
    update_user
)

app = FastAPI()

origins = [os.environ['FRONTEND_URL']]

app.add_middleware(
    CORSMiddleware,
    allow_origins= origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

SECRET_KEY = secrets.token_hex(32)
ALGORITHM = "HS256"

@app.post("/api/login")
async def login(request: Request):
    '''
    Returns the user who meets the credentials and a Json Web Token is generated.
    Returns: response: a JSON file containing informations about the user found and the JWT.
    '''

    data = await request.json()
    username = data.get("username")
    password = data.get("password")

    response = await fetch_one_user_psw(username, password)
    if response:
        user = await fetch_one_user(username)
        token = jwt.encode({"username": user.get('username'), "password": user.get('password'),
                            "email": user.get('email'),
                            "name": user.get('name'), "surname": user.get('surname')  },
                            SECRET_KEY, algorithm=ALGORITHM)
        return {"token": token}
    raise HTTPException(status_code=401, detail="Invalid username or password")

@app.get("/")
def read_root():
    '''
    Returns a set message; It is used to check if the application is reachable.
    Parameters:
        -
    
    Returns:
        msg (dict[str, str]): the string set to be returned.

    '''
    msg = {"msg": "LP base endpoint"}
    return msg

@app.delete("/user/{username}")
async def delete_user(username):
    '''
    Deletes a user by username
    
    Parameters:
        username: the username of the user to be deleted
    
    Returns:
        response: Succesfully deleted or username not found

    '''
    response = await remove_user(username)
    if response:
        return "Successfully deleted item"
    raise HTTPException(404, "Username not found")

@app.put("/user/{username}", response_model= User)
async def modify_user(username, update: UpdateModel):
    '''
    Modifies a user by username
    
    Parameters:
        username: the username of the user to be deleted
        update: new user data
    
    Returns:
        response: modified user or username not found

    '''
    response = await update_user(username,update.password,update.email,update.name,update.surname)
    if response:
        return response
    raise HTTPException(404, "not found")

@app.post("/user/", response_model = User)
async def post_user(user:User):
    '''
    Creates a user to the database if the username doesn't already exist
    
    Parameters:
        user (User): The user to add to the collection
    
    Returns:
        response: the user added, or error status

    '''
    existing_user = await fetch_one_user(user.username)
    if existing_user:
        raise HTTPException(status_code=400, detail="Username already exists")
    response = await create_user(user.dict())
    if response:
        return response
    raise HTTPException(400, response)

@app.get("/api/albums")
async def get_albums(fltr: str = ''):
    '''
    Returns all the albums in the database that satisfy the optional filter.
    
    Parameters: 
        fltr (str): the optional filter to apply to the album database.
    
    Returns:
        response: a JSON file containing informations about the albums found. 
      '''
    response = await fetch_albums(fltr)
    return response


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
