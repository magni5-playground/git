'''
Module used to hold all the utilities related to album objects.
'''
from pydantic import BaseModel
from .track import Track


class Album(BaseModel):
    '''
    Class used as a model for the data related to albums coming from the database.
    '''
    _id: dict
    album_id: str
    album_name: str
    album_artists: list[str]
    album_release_date: str
    album_cover_url: str
    tracks: list[Track]
