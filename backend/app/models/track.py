'''
Module used to hold all the utilities related to track objects.
'''
from pydantic import BaseModel


class Track(BaseModel):
    '''
    Class used as a model for the tracks data coming from the database.
    '''
    track_id: str
    track_name: str
    track_duration: int
