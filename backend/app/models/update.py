'''
Module used to hold the information related to the user.
'''

from pydantic import BaseModel

class User(BaseModel):
    '''
    Class for the User model: username, password, email, name, surname
    '''
    username: str
    password:str
    email:str
    name:str
    surname:str

class UpdateModel(BaseModel):
    '''
    Class for the UpdateModel model: password, email, name, surname
    '''
    password:str
    email:str
    name:str
    surname:str
