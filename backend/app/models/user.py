'''
Module used to hold all the utilities for the user.
'''

from pydantic import BaseModel

class User(BaseModel):
    '''
    Class used as a model for the data related to users.
    '''

    username: str
    password: str
    email: str
    name: str
    surname: str
