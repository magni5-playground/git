## Backend

The following instructions assume you are located in the current ```backend``` directory.

### Environment variables

List of the needed environment variables in order to correctly setup the application.

- FRONTEND_URL
- MONGODB_URL

Putting them in a ```.env``` file is suggested to enable them to be reused with Docker compose.

### Setup

Execute the following commands to setup the virtual environment and to install the dependencies.

- Linux / macOS
  ```bash
  python -m venv env
  source env/bin/activate
  pip install -r requirements.txt
  pip install -r dev-requirements.txt
  ```

- Windows
  ```bash
  python -m venv env
  env\Scripts\Activate
  pip install -r requirements.txt
  pip install -r dev-requirements.txt
  ```

The file ```requirements.txt``` contains all the dependencies useful for the project. The ```dev-requirements.txt``` one, instead, contains all the dependencies useful for the developers in order to perform code static analysis and testing.

### Run

Execute the following command to run the application.

```bash
uvicorn app.main:app --reload
```

### Verify

Execute the following command to do static analysis and linting of the source code.

```bash
pylint app
```

### Test

Execute one the following commands to execute the tests of interest.

```bash
pytest tests/unit
pytest tests/integration
```