'''
Module used to run integration tests on the different endpoint of the application.
'''
from fastapi.testclient import TestClient
from unittest.mock import patch

from app.main import app

client = TestClient(app)

def test_read_default_endpoint():
    '''
    Test to check if the base endpoint of the application is reachable.
    '''
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "LP base endpoint"}
    