'''
Module used to run unit tests on the different endpoint of the application.
'''
from fastapi.testclient import TestClient
from unittest.mock import patch

from app.main import app

client = TestClient(app)

def test_read_default_endpoint():
    '''
    Test to check if the base endpoint of the application is reachable.
    '''
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "LP base endpoint"}

def test_get_albums():
    '''
    Test to check if the application correctly retrieves albums object from the database.
    '''
    response = client.get("/api/albums")
    assert response.status_code == 200
    assert response.json() != {""}

# @patch('app.database.fetch_one_user', return_value=None)
# @patch('app.database.create_user', return_value={"username": "test_user", "password": "password123", "email": "test@example.com", "name": "Test", "surname": "User"})
# def test_post_user(create_user_mock, fetch_one_user_mock):
#     '''
#     Test to check if the application correctly creates a user inside the database.
#     '''
#     user_data = {
#         "username": "test_user",
#         "password": "password123",
#         "email": "test@example.com",
#         "name": "Test",
#         "surname": "User"
#     }
#     response = client.post("/user/", json=user_data)
#     assert response.status_code == 200
#     assert response.json() == user_data
