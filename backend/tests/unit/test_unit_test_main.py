'''
Module used to run tests on the different endpoint of the application.
'''
import sys, os

from fastapi.testclient import TestClient

# Aggiungi il percorso della cartella 'app' al sys.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

from app.main import app

client = TestClient(app)

def test_read_default_endpoint():
    '''
    Test to check if the base endpoint of the application is reachable.
    '''
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "LP base endpoint"}

def test_login():
    '''
    Test to check if the application correctly handles login requests.
    '''
    login_data = {"username": "aaa", "password": "bbb"}

    response = client.post("/api/login", json=login_data)

    assert response.status_code == 200

    assert "token" in response.json()
