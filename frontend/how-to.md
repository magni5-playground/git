## Frontend

The following instructions assume you are located in the current ```frontend``` directory.

### Environment variables

List of the needed environment variables in order to correctly setup the application.

- REACT_APP_BACKEND_URL

Putting them in a ```.env``` file is suggested to enable them to be reused with Docker compose.

### Setup

Execute the following commands to setup and to install the dependencies.

```bash
npm install
```

### Run

Execute the following command to run the application.

```bash
npm start
```

### Verify

Execute the following only the first time to setup the linter.

```bash
npm install eslint @react-native-community/eslint-config
```

Execute the following command to do static analysis and linting of the source code.

```bash
node_modules/eslint/bin/eslint.js src
```

### Test

Execute one the following commands to execute the tests of interest.

```bash
npm test src/tests/unit
npm test src/tests/integration
```