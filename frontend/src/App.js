import "./App.css";
import React, { useState } from "react";
import { AuthProvider } from "./components/AuthContext";
import "bootstrap/dist/css/bootstrap.min.css";
import ScrollToTopButton from "./components/ScrollToTopButton";
import Home from "./components/Home";
import Sidebar from "./components/Sidebar";
import Registration from "./components/Registration";
import Login from "./components/Login";
import UserProfile from "./components/UserProfile";

function App() {
  // toggle sidebar visibility
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  // toggle sidebar visibility
  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  let Component;
  switch (window.location.pathname) {
    case "/":
      Component = Home;
      break;

    case "/Registration":
      Component = Registration;
      break;

    case "/Login":
      Component = Login;
      break;

    case "/UserProfile":
      Component = UserProfile;
      break;

    default:
      Component = Home;
      break;
  }

  return (
    <AuthProvider>
      <div className="App">
        <header className="App-header"></header>
      </div>
      <div data-testid="app" className="App">
        <Sidebar isOpen={isSidebarOpen} onClose={toggleSidebar} />
        <div
          className={isSidebarOpen ? "overlay" : ""}
          onClick={toggleSidebar}
        ></div>
        <div className="background"></div>
        <div className="background-color"></div>
        <Component toggleSidebar={toggleSidebar} />
        {Component === Home && <ScrollToTopButton />}
      </div>
    </AuthProvider>
  );
}

export default App;
