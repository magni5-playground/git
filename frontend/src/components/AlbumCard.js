import React, { useState } from "react";
import "./AlbumCard.css";

function AlbumCard({ album, onClick }) {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleClick = () => {
    if (onClick) {
      onClick(); // Call the onClick function passed from the parent component
    }
  };

  return (
    <div className="album-container">
      <div
        data-testid="album-card"
        key={album.album_id}
        className={`album-card ${isHovered ? "hovered" : ""}`}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={handleClick}
      >
        <div className="album-cover">
          <img src={album.album_cover_url} alt={album.album_name} />
        </div>
        <div className="album-details">
          <h2>{album.album_name}</h2>
          <p>Artists: {album.album_artists.join(", ")}</p>
          <p>Release Date: {album.album_release_date}</p>
          <p>
            Number of Tracks: {JSON.parse(JSON.stringify(album.tracks)).length}
          </p>
        </div>
      </div>
    </div>
  );
}
export default AlbumCard;
