import React, { useState } from "react";
import AlbumCard from "./AlbumCard";
import AlbumDetail from "./AlbumDetail";

function AlbumCardListView(props) {
  const [selectedAlbum, setSelectedAlbum] = useState(null);

  const handleAlbumClick = (album) => {
    setSelectedAlbum(album);
  };

  const handleCloseAlbumDetail = () => {
    setSelectedAlbum(null); // Set selectedAlbum to null to close the AlbumDetail component
  };

  return (
    <div>
      {selectedAlbum ? (
        <AlbumDetail album={selectedAlbum} onClose={handleCloseAlbumDetail} />
      ) : (
        <ul>
          {props.albumList.map((album) => (
            <AlbumCard
              key={album.album_id}
              album={album}
              onClick={() => handleAlbumClick(album)}
            />
          ))}
        </ul>
      )}
    </div>
  );
}

export default AlbumCardListView;
