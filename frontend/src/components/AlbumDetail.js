import React, { useEffect, useRef, useState } from "react";
import "./AlbumDetail.css";

function AlbumDetail(props) {
  const [isMounted, setIsMounted] = useState(false);
  const albumDetailRef = useRef(null); // Ref for AlbumDetail container

  useEffect(() => {
    setIsMounted(true); // Set isMounted to true when the component mounts

    // Clean up when the component unmounts
    return () => {
      setIsMounted(false);
    };
  }, []);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        isMounted &&
        albumDetailRef.current &&
        !albumDetailRef.current.contains(event.target)
      ) {
        props.onClose();
      }
    };

    // Add event listener when the component is mounted and visible
    if (isMounted) {
      document.addEventListener("click", handleClickOutside);
    }

    // Clean up the event listener when the component unmounts or is no longer visible
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, [isMounted, props]);

  return (
    <div className="album-detail-container" ref={albumDetailRef}>
      <div className="album-cover-img">
        <img src={props.album.album_cover_url} alt={props.album.album_name} />
      </div>
      <div className="album-infos">
        <h2>{props.album.album_name}</h2>
        <p>Artists: {props.album.album_artists.join(", ")}</p>
        <p>Release Date: {props.album.album_release_date}</p>
        <p>Number of Tracks: {props.album.tracks.length}</p>
        <div className="tracks-list">
          <h3>Tracks:</h3>
          <ul>
            {props.album.tracks.map((track, index) => (
              <li key={index}>
                {track.track_name} - {
                  Math.floor(Math.floor(track.track_duration / 1000) / 60)
                }:{
                  (Math.floor(track.track_duration / 1000) % 60).toString().padStart(2, '0')
                }              
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default AlbumDetail;
