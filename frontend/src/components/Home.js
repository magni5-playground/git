import React, { useState, useEffect } from "react";
import axios from "axios";
import Navbar from "./Navbar";
import AlbumCardListView from "./AlbumCardListView";
import "./Home.css";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

function Home({ toggleSidebar }) {
  // album list setup
  const [albumList, setAlbumList] = useState([]);

  // fetch albums informations
  const fetchAlbums = (filter = "") => {
    axios
      .get(`${BACKEND_URL}/api/albums${filter ? `?fltr=${filter}` : ""}`)
      .then((res) => {
        setAlbumList(res.data);
      })
      .catch((error) => {
        console.error("Error fetching albums:", error);
      });
  };

  // initial album reading
  useEffect(() => {
    fetchAlbums();
  }, []);

  return (
    <div data-testid="home" className="Home">
      <Navbar onSearch={fetchAlbums} toggleSidebar={toggleSidebar} />
      {albumList.length > 0 && <AlbumCardListView albumList={albumList} />}
    </div>
  );
}

export default Home;
