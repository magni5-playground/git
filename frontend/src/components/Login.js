import React, { useState } from "react";
import axios from "axios";
import "./Login.css";
import appLogo from "../img/appLogo.png";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState("");

  //const { setToken } = useContext(AuthContext);

  const handleLogin = async () => {
    try {
      const response = await axios.post(BACKEND_URL + '/api/login', {
        username: username,
        password: password,
      });

      const token = response.data.token;
      localStorage.setItem("token", token);
      setToken(token);

      console.log("Login successful" + token);
      window.location.href = "/Home";
    } catch (error) {
      console.error("Login error:", error.message);
    }
  };

  const redirectToHomepage = () => {
    window.location.href = "/"; // Redirect to the homepage
  };

  return (
    <div className="registration-container">
      <a href="/" onClick={redirectToHomepage}>
        <img
          src={appLogo}
          alt="Homepage"
          className="homepage-image"
          onClick={redirectToHomepage}
        />
      </a>
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form>
            <h2 className="registration-header">Login</h2>
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                type="text"
                className="form-control"
                id="username"
                placeholder="Username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <div className="input-group mb-3">
                <input
                  type={showPassword ? "text" : "password"}
                  className="form-control"
                  id="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {/* <button
                  className="btn btn-outline-secondary"
                  type="button"
                  onClick={() => setShowPassword(!showPassword)}
                >
                  mostra
                  {showPassword ? (
                    <i className="bi bi-eye-slash"></i>
                  ) : (
                    <i className="bi bi-eye"></i>
                  )}
                </button> */}
              </div>
            </div>
            <button
              type="button"
              className="btn btn-primary btn-lg btn-block"
              onClick={handleLogin}
            >
              Login
            </button>
          </form>
        </div>
      </div>
      {error && <p className="error-message">{error}</p>}
    </div>
  );
};

export default Login;
