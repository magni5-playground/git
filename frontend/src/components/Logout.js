import React from "react";

const Logout = ({ onLogout }) => {
  let token = localStorage.getItem("token");

  if (token) {
    return (
      <div>
        <button onClick={onLogout}>Logout</button>
      </div>
    );
  } else return <div></div>;
};

export default Logout;
