import React, { useState, useEffect } from "react";
import appLogo from "../img/appLogo.png";
import missingUserImage from "../img/missingUserImage.png";
import "./Navbar.css";
import { jwtDecode } from "jwt-decode";

function Navbar({ onSearch, toggleSidebar }) {
  const [searchQuery, setSearchQuery] = useState("");
  const [userData, setUserData] = useState(null);

  let token = localStorage.getItem("token");

  useEffect(() => {
    const decodeToken = (token) => {
      if (token) {
        try {
          const decoded = jwtDecode(token);
          return decoded;
        } catch (error) {
          console.error("Error decoding token:", error);
          return null;
        }
      }
    };

    const userDataFromToken = decodeToken(token);
    setUserData(userDataFromToken);
    console.log(userDataFromToken);
  }, [token]);

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
    onSearch(e.target.value);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
    }
  };

  const handleProfileClick = () => {
    toggleSidebar();
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light custom-navbar">
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          <img className="appLogo" src={appLogo} alt="Logo" height="50" />
        </a>
        <form className="d-flex" onKeyDown={handleKeyDown}>
          <div className="search-container">
            <input
              className="form-control search-input"
              type="search"
              placeholder="Search"
              aria-label="Search"
              value={searchQuery}
              onChange={handleSearch}
              style={{ width: "800px" }}
            />
            <i className="fas fa-search search-icon"></i>
          </div>
        </form>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <div
              data-testid="sidebar-toggle"
              className="nav-link"
              onClick={handleProfileClick}
            >
              {userData && (
                <div>
                  <a>Welcome </a>
                  {userData.username}
                </div>
              )}

              <img
                src={missingUserImage}
                alt="User"
                className="user-profile-picture"
              />
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;
