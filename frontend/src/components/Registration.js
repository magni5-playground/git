import axios from "axios";
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Registration.css";
import appLogo from "../img/appLogo.png";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

function Registration() {
  //const [userList, setUserList] = useState([{}]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    const timer = setTimeout(() => {
      setError("");
    }, 5000);

    return () => clearTimeout(timer);
  }, [error]);

  const validateEmail = (email) => {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(String(email).toLowerCase());
  };

  //Post a user
  const addUserHandler = () => {
    if (!validateEmail(email)) {
      setError("Invalid email address");
      return;
    }
    if (!username || !password || !email || !name || !surname) {
      setError("Please fill out all required fields.");
      return;
    }
    axios
      .post(BACKEND_URL + '/user/', {
        username: username,
        password: password,
        email: email,
        name: name,
        surname: surname,
      })
      .then((res) => {
        console.log(res);
        window.location.href = "/";
        setError("");
      })
      .catch((error) => {
        console.error("Error adding user:", error);
        if (error.response && error.response.status === 400) {
          setError(
            "Username already exists. Please choose a different username."
          );
        } else {
          setError("An error occurred. Please try again later.");
        }
      });
  };

  const redirectToHomepage = () => {
    window.location.href = "/"; // Redirect to the homepage
  };

  return (
    <div className="registration-container">
      <a href="/" onClick={redirectToHomepage}>
        <img
          src={appLogo}
          alt="Homepage"
          className="homepage-image"
          onClick={redirectToHomepage}
        />
      </a>
      <h1 className="registration-header">Register</h1>
      <div className="registration-form">
        <input
          type="text"
          className="form-input"
          onChange={(event) => setUsername(event.target.value)}
          placeholder="Username"
        />
        <input
          type="password"
          className="form-input"
          onChange={(event) => setPassword(event.target.value)}
          placeholder="Password"
        />
        <input
          type="email"
          className="form-input"
          onChange={(event) => setEmail(event.target.value)}
          placeholder="Email"
        />
        <input
          type="text"
          className="form-input"
          onChange={(event) => setName(event.target.value)}
          placeholder="Name"
        />
        <input
          type="text"
          className="form-input"
          onChange={(event) => setSurname(event.target.value)}
          placeholder="Surname"
        />
        <button className="submit-button" onClick={addUserHandler}>
          Add User
        </button>
      </div>
      {error && <p className="error-message">{error}</p>}
    </div>
  );
}

export default Registration;
