import React from "react";
import "./ScrollToTopButton.css";

function ScrollToTopButton() {
  // button behaviour
  const handleScrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <button onClick={handleScrollToTop} className="scroll-to-top-button">
      <i data-testid="arrow-up-icon" className="fas fa-arrow-up"></i>
    </button>
  );
}

export default ScrollToTopButton;
