import React, { useEffect, useRef } from "react";
import "./Sidebar.css";
import Logout from "./Logout";
import UserProfileButton from "./UserProfileButton";

const Sidebar = ({ isOpen, onClose }) => {
  const sidebarRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        isOpen &&
        sidebarRef.current &&
        !sidebarRef.current.contains(event.target)
      ) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOpen, onClose]);

  const handleLogout = () => {
    localStorage.removeItem("token");

    window.location.href = "/Home";
  };

  return (
    isOpen && (
      <div
        data-testid="sidebar"
        ref={sidebarRef}
        className={`sidebar ${isOpen ? "show" : ""}`}
      >
        <ul>
          <li>
            <a href="/Registration">Sign Up</a>
          </li>
          <li>
            <a href="/Login">Login</a>
          </li>
          <li>
            <a href="/UserProfile">
              <UserProfileButton />
            </a>
          </li>
          <li>
            <Logout onLogout={handleLogout} />
          </li>
        </ul>
      </div>
    )
  );
};

export default Sidebar;
