import axios from 'axios'
import React from 'react'

function TrackItem(props) {
    return (
        <div>
            <p>
                <span style={{ fontWeight: 'bold, underline' }}>
                    {props.track.track_name}
                </span> :
                {props.track.track_duration}
            </p>
        </div>
    )
}

export default TrackItem;