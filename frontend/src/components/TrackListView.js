import TrackItem from "./Track"

function TrackView(props) {
    return (
        <span>
            {props.trackList.map((track, index) => <span>{index + 1}) <TrackItem track={track} /></span>)}
        </span>
    )
}

export default TrackView