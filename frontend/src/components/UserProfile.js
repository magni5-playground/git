import React, { useState, useEffect } from "react";
import { jwtDecode } from "jwt-decode";
import axios from "axios";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

const UserProfile = () => {
  //const { token } = useContext(AuthContext);
  //const [token, setToken] = useState("");
  const [userData, setUserData] = useState(null);
  const [editedData, setEditedData] = useState({
    password: "",
    email: "",
    name: "",
    surname: "",
  });

  let token = localStorage.getItem("token");

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedData({ ...editedData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(editedData);
    try {
      await axios.put(
        `${BACKEND_URL}/user/${userData.username}`,
        editedData,
        {
          headers: {
            "Content-Type": "application/json",
            // You may need to include authorization headers if required by your API
            // 'Authorization': `Bearer ${token}`
          },
        }
      );

      console.log("User data updated successfully");

      // Optionally, you can update the local state or perform any other actions after updating user data
    } catch (error) {
      console.error("Error updating user data:", error.response.data);
    } finally {
      localStorage.clear();
      window.location.href = "/";
    }
  };

  const handleDelete = async () => {
    try {
      await axios.delete(`${BACKEND_URL}/user/${userData.username}`, {
        headers: {
          "Content-Type": "application/json",
        },
      });

      console.log("User deleted successfully");
      localStorage.clear();
      window.location.href = "/";
    } catch (error) {
      console.error("Error deleting user:", error);
    }
  };

  useEffect(() => {
    const decodeToken = (token) => {
      try {
        const decoded = jwtDecode(token);
        return decoded;
      } catch (error) {
        console.error("Error decoding token:", error);
        return null;
      }
    };

    // Move the declaration of userDataFromToken inside the useEffect
    const userDataFromToken = decodeToken(token);
    setUserData(userDataFromToken);
    editedData.password = userDataFromToken.password;
    editedData.email = userDataFromToken.email;
    editedData.name = userDataFromToken.name;
    editedData.surname = userDataFromToken.surname;

    console.log(userDataFromToken);
  }, [token]);

  // You can use the token here for any API calls or other purposes
  return (
    <div className="container">
      <h2 className="my-4">User Profile</h2>
      {userData && (
        <div className="mb-3">
          <label className="form-label">Username:</label>
          <span className="badge bg-secondary">{userData.username}</span>
        </div>
      )}
      {userData && (
        <form>
          <div className="mb-3">
            <label htmlFor="password" className="form-label">
              Password:
            </label>
            <input
              type="password"
              className="form-control"
              id="password"
              name="password"
              value={editedData.password || userData.password}
              onChange={handleInputChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label">
              Email:
            </label>
            <input
              type="email"
              className="form-control"
              id="email"
              name="email"
              value={editedData.email || userData.email}
              onChange={handleInputChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="name" className="form-label">
              Name:
            </label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={editedData.name || userData.name}
              onChange={handleInputChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="surname" className="form-label">
              Surname:
            </label>
            <input
              type="text"
              className="form-control"
              id="surname"
              name="surname"
              value={editedData.surname || userData.surname}
              onChange={handleInputChange}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary me-2"
            onClick={handleSubmit}
          >
            Save Changes
          </button>
          <button
            type="button"
            className="btn btn-danger"
            onClick={handleDelete}
          >
            Delete User
          </button>
        </form>
      )}
    </div>
  );
};

export default UserProfile;
