import React from "react";

const UserProfileButton = ({ onUserProfile }) => {
  let token = localStorage.getItem("token");

  if (token) {
    return (
      <div>
        <button onClick={onUserProfile}>User Profile</button>
      </div>
    );
  } else return <div></div>;
};

export default UserProfileButton;
