import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import AlbumDetail from "../../components/AlbumDetail";

// Mock the onClose function
const onCloseMock = jest.fn();

// Sample album data for testing
const sampleAlbum = {
  album_name: "Sample Album",
  album_artists: ["Artist 1", "Artist 2"],
  album_release_date: "2022-04-09",
  album_cover_url: "sample_cover_url",
  tracks: [
    { track_name: "Track 1", track_duration: "60000" },
    { track_name: "Track 2", track_duration: "60000" },
  ],
};

describe("AlbumDetail Component - Integration Tests", () => {
  test("calls onClose when clicking outside the component", () => {
    render(<AlbumDetail album={sampleAlbum} onClose={onCloseMock} />);

    // Simulate click outside the component
    fireEvent.click(document.body);

    // Check if onClose is called
    expect(onCloseMock).toHaveBeenCalled();
  });

  test("does not call onClose when clicking inside the component", () => {
    render(<AlbumDetail album={sampleAlbum} onClose={onCloseMock} />);

    // Simulate click inside the component
    fireEvent.click(screen.getByText("Sample Album"));

    // Check if onClose is not called
    expect(onCloseMock).not.toHaveBeenCalled();
  });
});
