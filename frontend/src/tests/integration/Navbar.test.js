import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Navbar from "../../components/Navbar";

describe("Navbar Component - Integration Tests", () => {
  it("renders logo, search input, and profile icon", () => {
    render(<Navbar />);
    const logo = screen.getByAltText("Logo");
    const searchInput = screen.getByPlaceholderText("Search");
    const profileIcon = screen.getByAltText("User");
    expect(logo).toBeInTheDocument();
    expect(searchInput).toBeInTheDocument();
    expect(profileIcon).toBeInTheDocument();
  });
});
