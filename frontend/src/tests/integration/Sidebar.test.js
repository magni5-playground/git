import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Sidebar from "../../components/Sidebar";

describe("Sidebar Component - Integration Tests", () => {
  it("renders the sidebar when isOpen is true", () => {
    render(<Sidebar isOpen={true} onClose={() => {}} />);
    const sidebar = screen.getByTestId("sidebar");
    expect(sidebar).toBeInTheDocument();
  });

  it("does not render the sidebar when isOpen is false", () => {
    render(<Sidebar isOpen={false} onClose={() => {}} />);
    const sidebar = screen.queryByTestId("sidebar");
    expect(sidebar).not.toBeInTheDocument();
  });
});
