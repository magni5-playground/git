import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import axios from "axios";
import App from "../../App";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

jest.mock("axios");

describe("Integration Tests", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Clear all mocks before each test
  });

  it("renders the App component", () => {
    axios.get.mockResolvedValueOnce({});
    render(<App />);
    expect(axios.get).toHaveBeenCalledWith(BACKEND_URL + '/api/albums');
    const appElement = screen.getByTestId("app");
    expect(appElement).toBeInTheDocument();
  });

  it("fetches albums on initial render", async () => {
    const mockData = [
      {
        album_id: 1,
        album_name: "Test Album",
        album_artists: ["Artist 1", "Artist 2"],
        album_cover_url: "test_url",
        album_release_date: "2024-01-01",
        tracks: [{}, {}],
      },
    ];
    axios.get.mockResolvedValueOnce({ data: mockData });
    render(<App />);
    expect(axios.get).toHaveBeenCalledWith(BACKEND_URL + '/api/albums');
    expect(await screen.findByText("Test Album")).toBeInTheDocument(); // Adjusted text to match the album name
  });
});
