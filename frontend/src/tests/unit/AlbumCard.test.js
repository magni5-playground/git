import React from "react";
import { render, screen } from "@testing-library/react";
import AlbumCard from "../../components/AlbumCard";
import "@testing-library/jest-dom/extend-expect";

describe("AlbumCard Component", () => {
  // Unit Tests
  describe("Unit Tests", () => {
    it("renders album name correctly", () => {
      const album = {
        album_id: 1,
        album_name: "Test Album",
        album_artists: ["Artist 1", "Artist 2"],
        album_cover_url: "test_url",
        album_release_date: "2024-01-01",
        tracks: [{}, {}], // Dummy tracks
      };

      render(<AlbumCard album={album} />);
      expect(screen.getByText(album.album_name)).toBeInTheDocument();
    });

    it("renders correct number of tracks", () => {
      const album = {
        album_id: 1,
        album_name: "Test Album",
        album_artists: ["Artist 1", "Artist 2"],
        album_cover_url: "test_url",
        album_release_date: "2024-01-01",
        tracks: [{}, {}, {}], // Dummy tracks
      };

      render(<AlbumCard album={album} />);
      expect(screen.getByText("Number of Tracks: 3")).toBeInTheDocument();
    });
  });
});
