import React from "react";
import { render, screen } from "@testing-library/react";
import AlbumCardListView from "../../components/AlbumCardListView";

describe("AlbumCardListView Component", () => {
  it("renders without crashing", () => {
    const albumList = [
      {
        album_id: 1,
        album_name: "Test Album 1",
        album_artists: ["Artist 1"],
        album_cover_url: "test_url_1",
        album_release_date: "2024-01-01",
        tracks: [{}],
      },
      {
        album_id: 2,
        album_name: "Test Album 2",
        album_artists: ["Artist 2"],
        album_cover_url: "test_url_2",
        album_release_date: "2024-01-02",
        tracks: [{}],
      },
    ];

    render(<AlbumCardListView albumList={albumList} />);
  });

  it("renders all albums in the list", () => {
    const albumList = [
      {
        album_id: 1,
        album_name: "Test Album 1",
        album_artists: ["Artist 1"],
        album_cover_url: "test_url_1",
        album_release_date: "2024-01-01",
        tracks: [{}],
      },
      {
        album_id: 2,
        album_name: "Test Album 2",
        album_artists: ["Artist 2"],
        album_cover_url: "test_url_2",
        album_release_date: "2024-01-02",
        tracks: [{}],
      },
    ];

    render(<AlbumCardListView albumList={albumList} />);

    const albumCards = screen.getAllByTestId("album-card");

    expect(albumCards).toHaveLength(albumList.length);
  });
});
