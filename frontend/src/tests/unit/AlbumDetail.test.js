import React from "react";
import { render, screen } from "@testing-library/react";
import AlbumDetail from "../../components/AlbumDetail";
import "@testing-library/jest-dom";

// Mock the onClose function
const onCloseMock = jest.fn();

// Sample album data for testing
const sampleAlbum = {
  album_name: "Sample Album",
  album_artists: ["Artist 1", "Artist 2"],
  album_release_date: "2022-04-09",
  album_cover_url: "sample_cover_url",
  tracks: [
    { track_name: "Track 1", track_duration: "60000" },
    { track_name: "Track 2", track_duration: "60000" },
  ],
};

describe("AlbumDetail Component - Unit Tests", () => {
  test("renders album details", () => {
    render(<AlbumDetail album={sampleAlbum} onClose={onCloseMock} />);

    // Check if album details are rendered
    expect(screen.getByText("Sample Album")).toBeInTheDocument();
    expect(screen.getByText("Artists: Artist 1, Artist 2")).toBeInTheDocument();
    expect(screen.getByText("Release Date: 2022-04-09")).toBeInTheDocument();
    expect(screen.getByText("Number of Tracks: 2")).toBeInTheDocument();
    expect(screen.getByText("Track 1 - 1:00")).toBeInTheDocument();
    expect(screen.getByText("Track 2 - 1:00")).toBeInTheDocument();
  });
});
