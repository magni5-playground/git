import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Navbar from "../../components/Navbar";

describe("Navbar Component - Unit Tests", () => {
  it("renders without crashing", () => {
    render(<Navbar />);
  });

  it("updates searchQuery state when typing in the search input", () => {
    const onSearchMock = jest.fn();
    render(<Navbar onSearch={onSearchMock} />);
    const searchInput = screen.getByPlaceholderText("Search");
    fireEvent.change(searchInput, { target: { value: "test" } });
    expect(searchInput.value).toBe("test");
  });

  it("calls onSearch prop with search query when typing in the search input", () => {
    const onSearchMock = jest.fn();
    render(<Navbar onSearch={onSearchMock} />);
    const searchInput = screen.getByPlaceholderText("Search");
    fireEvent.change(searchInput, { target: { value: "test" } });
    expect(onSearchMock).toHaveBeenCalledWith("test");
  });

  it("calls toggleSidebar prop when profile icon is clicked", () => {
    const toggleSidebarMock = jest.fn();
    render(<Navbar toggleSidebar={toggleSidebarMock} />);
    const profileIcon = screen.getByAltText("User");
    fireEvent.click(profileIcon);
    expect(toggleSidebarMock).toHaveBeenCalled();
  });
});
