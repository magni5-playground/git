import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import axios from "axios";
import Registration from "../../components/Registration";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

jest.mock("axios");

describe("Registration Component", () => {
  it("renders without crashing", () => {
    render(<Registration />);
  });

  it("submits user registration form", async () => {
    const mockUser = {
      username: "testuser",
      password: "testpassword",
      email: "test@example.com",
      name: "Test",
      surname: "User",
    };
    axios.post.mockResolvedValueOnce({ data: {} });
    render(<Registration />);
    fireEvent.change(screen.getByPlaceholderText("Username"), {
      target: { value: mockUser.username },
    });
    fireEvent.change(screen.getByPlaceholderText("Password"), {
      target: { value: mockUser.password },
    });
    fireEvent.change(screen.getByPlaceholderText("Email"), {
      target: { value: mockUser.email },
    });
    fireEvent.change(screen.getByPlaceholderText("Name"), {
      target: { value: mockUser.name },
    });
    fireEvent.change(screen.getByPlaceholderText("Surname"), {
      target: { value: mockUser.surname },
    });
    fireEvent.click(screen.getByText("Add User"));
    await waitFor(() => expect(axios.post).toHaveBeenCalledTimes(1));
    expect(axios.post).toHaveBeenCalledWith(
      BACKEND_URL + "/user/",
      mockUser
    );
    //expect(window.location.href).toEqual("/");
  });

  // Add more unit tests for validation and error handling as needed
});
