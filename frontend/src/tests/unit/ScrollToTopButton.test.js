import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import ScrollToTopButton from "../../components/ScrollToTopButton";

describe("ScrollToTopButton Component - Unit Tests", () => {
  it("renders without crashing", () => {
    render(<ScrollToTopButton />);
  });

  it("scrolls to the top when the button is clicked", () => {
    window.scrollTo = jest.fn(); // Mock window.scrollTo function
    render(<ScrollToTopButton />);
    const button = screen.getByRole("button");
    fireEvent.click(button);
    expect(window.scrollTo).toHaveBeenCalledWith({
      top: 0,
      behavior: "smooth",
    });
  });
});
