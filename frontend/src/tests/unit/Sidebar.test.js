import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Sidebar from "../../components/Sidebar";

describe("Sidebar Component - Unit Tests", () => {
  it("renders without crashing", () => {
    render(<Sidebar isOpen={true} onClose={() => {}} />);
  });

  it("closes the sidebar when clicked outside", () => {
    const onCloseMock = jest.fn();
    render(<Sidebar isOpen={true} onClose={onCloseMock} />);
    fireEvent.mouseDown(document);
    expect(onCloseMock).toHaveBeenCalled();
  });
});
