import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import App from "../../App";
import "@testing-library/jest-dom";
import axios from "axios";

const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;

jest.mock("axios");

// Unit Tests
describe("Unit Tests", () => {
  it("renders without crashing", () => {
    axios.get.mockResolvedValueOnce({});
    render(<App />);
    expect(axios.get).toHaveBeenCalledWith(BACKEND_URL + "/api/albums");
  });

  it("toggles sidebar visibility when calling toggleSidebar function", () => {
    axios.get.mockResolvedValueOnce({});
    render(<App />);
    expect(axios.get).toHaveBeenCalledWith(BACKEND_URL + "/api/albums");
    fireEvent.click(screen.getByTestId("sidebar-toggle"));
    expect(screen.getByTestId("sidebar")).toBeInTheDocument();
  });
});
