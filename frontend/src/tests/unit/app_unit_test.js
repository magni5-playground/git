import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import App from "../../App";
import "@testing-library/jest-dom";
import axios from "axios";

jest.mock("axios");

describe("Unit Tests", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
  });
});
