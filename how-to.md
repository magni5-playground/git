# How-to

## Backend

[backend](backend/how-to.md)

## Frontend

[backend](frontend/how-to.md)

## Docker

If using Docker remember to put the needed environment variables in a ```.env``` file for each application component.

Then execute the following command to setup and run all the components.

```bash
docker compose up
```

Enjoy.