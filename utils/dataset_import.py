'''
This script is useful to massively import a dataset into the MongoDB instance.

It is needed to:
    - Set the MONGODB_URL as an environment variable.
    - Change the FILE_PATH constant to match the csv file containing the dataset to import.
'''

import os

import pandas as pd
from pymongo import MongoClient

mongo_url = os.environ.get('MONGODB_URL')
client = MongoClient(mongo_url)
db = client.lp
collection = db.albums

FILE_PATH = 'path_to_dataset.csv'
data = pd.read_csv(FILE_PATH)
data_dict = data.to_dict(orient='records')

collection.insert_many(data_dict)

print('Dataset imported successfully.')
